from explorador.TipoComponente import TipoComponente


class Analizador:

    def __init__(self, _lista_componentes):
        self.lista_componentes = _lista_componentes
        self.cantidad = len(self.lista_componentes)
        self.pos_actual = 0
        self.componente_actual = self.lista_componentes[0]

    def analizar(self):
        self.__analizar_programa()

    def __analizar_programa(self):

        # Los comentarios se omiten desde el Explorador

        # Múltiples asignaciones o funciones
        while (True):

            if self.componente_actual.tipo == TipoComponente.IDENTIFICADOR:
                # Si es asignación
                if self.__siguiente_componente().tipo == TipoComponente.ASIGNACION:
                    self.__analizar_asignacion()
                # Si es función
                elif self.__siguiente_componente().tipo == TipoComponente.FUNCION:
                    print("Entro a la funcion")
                    self.__analizar_funcion()

            else:
                break

    def __analizar_asignacion(self):
        self.__verificar_identificador()

        self.__verificar('->')

        if self.componente_actual.tipo in [TipoComponente.ENTERO, TipoComponente.FLOTANTE, TipoComponente.VALOR_VERDAD,
                                           TipoComponente.TEXTO]:
            self.__analizar_literal()
        elif self.componente_actual.texto == '(':
            self.__analizar_expresion_matematica()
        else:
            self.__analizar_invocacion()

    def __analizar_funcion(self):
        """
        Función ::= (Comentario)? mae Identificador (Parámetros) { Instrucción+ }
        """
        # Comentario con doble check azul dela muerte... (ignorado)
        # Esta sección es obligatoria en este orden
        self.__verificar_identificador()
        self.__verificar('-->')
        self.__verificar('(')
        self.__analizar_parametros()
        self.__verificar(')')
        self.__analizar_bloque_instrucciones()

    def __analizar_parametros(self):
        """
        Parametros ::= Valor (, Valor)+
        """
        # Fijo un valor tiene que haber
        self.__analizar_valor()

        while (self.componente_actual.texto == ','):
            self.__verificar(',')
            self.__analizar_valor()

    def __analizar_bloque_instrucciones(self):
        """
        Este es nuevo y me lo inventé para simplicicar un poco el código...
        correspondería actualizar la gramática si lo consideran necesario.

        BloqueInstrucciones ::= { Instrucción+ }
        """

        # Obligatorio
        self.__verificar('{')

        # mínimo una
        self.__analizar_instruccion()

        # Acá
        while self.componente_actual.texto in ['^', '?', '?:', ':', '>>', '++'] \
                or self.componente_actual.tipo == TipoComponente.IDENTIFICADOR:
            self.__analizar_instruccion()

        # Obligatorio
        self.__verificar('}')

    def __analizar_instruccion(self):
        """
        Instrucción ::= (Repetición | Bifurcación | Asignación | Invocación | Retorno | Error | Comentario )

        Acá hay un error en la gramática por que no reconoce las
        Invocaciones por la falta de corregir un error en la gramática LL

        Invocación y Asignación ... ambas dos inician con un Identificador
        y acá no se sabe por cuál empezar.
        ...
        La solución en código que yo presentó acá esta sería como algo así

        Instrucción ::= (Repetición | Bifurcación | (Asignación | Invocación) | Retorno | Error | Comentario )

                                                    ^                       ^
        Ojo los paréntesis extra                    |                       |
        """
        if self.componente_actual.texto == '^':
            self.__analizar_repeticion()

        elif self.componente_actual.texto == '?':
            self.__bifu_si()

        elif self.componente_actual.texto == '?:':
            self.__bifu_TalVez()

        elif self.componente_actual.texto == ':':
            self.__SiNo()
        elif self.componente_actual.texto == '++':
            self.__concatenar()

        elif self.componente_actual.tipo == TipoComponente.IDENTIFICADOR:

            if self.__siguiente_componente().tipo == TipoComponente.ASIGNACION:
                self.__analizar_asignacion()
            else:
                self.__analizar_invocacion()

        elif self.componente_actual.texto == '>>':
            self.__analizar_retorno()

        else:  # Muy apropiado el chiste de ir a revisar si tiene error al último.
            print(self.componente_actual)
            self.__analizar_error()

    def __concatenar(self):
        self.__verificar("++")
        if self.componente_actual.tipo == TipoComponente.IDENTIFICADOR:
            self.__verificar_tipo(TipoComponente.IDENTIFICADOR)
        else:
            self.__analizar_literal()

    def __analizar_error(self):
        if self.componente_actual.tipo == TipoComponente.ERRORVARIABLE:
            print(self.componente_actual)
            self.__avanzar_componente()
            self.__analizar_valor()
            raise SyntaxError(str(self.componente_actual) + '  Error a la hora de definir variable')
        elif self.componente_actual.tipo == TipoComponente.ERRORFLOTANTE:
            self.__avanzar_componente()
            self.__analizar_valor()
            raise SyntaxError(str(self.componente_actual) + '  Error a la hora de definir Numeros Flotantes')
        else:
            self.__avanzar_componente()
            self.__analizar_valor()
            raise SyntaxError(str(self.componente_actual) + ' Error en algun lugar')



    def __analizar_retorno(self):
        """
        Retorno :: >> (Valor)?
        """
        self.__verificar('>>')

        # Este hay que validarlo para evitar el error en caso de que no
        # aparezca
        if self.componente_actual.tipo in [TipoComponente.IDENTIFICADOR, TipoComponente.ENTERO, TipoComponente.FLOTANTE,
                                           TipoComponente.VALOR_VERDAD, TipoComponente.TEXTO]:
            self.__analizar_valor()


    def __bifu_si(self):
        self.__verificar('?')
        self.__verificar('(')
        self.__analizar_condicion()
        self.__verificar(')')
        self.__analizar_bloque_instrucciones()

    def __bifu_TalVez(self):
        self.__verificar('?:')
        self.__verificar('(')
        self.__analizar_condicion()
        self.__verificar(')')
        self.__analizar_bloque_instrucciones()

    def __SiNo(self):
        self.__verificar(':')
        self.__analizar_bloque_instrucciones()

    def __analizar_repeticion(self):
        """
        Repetición ::= upee ( Condición ) { Instrucción+ }
        """

        # Todos presentes en ese orden... sin opciones
        self.__verificar('^')
        self.__verificar('(')
        self.__analizar_condicion()
        self.__verificar(')')
        self.__analizar_bloque_instrucciones()

    # Ignorado el comentario

    def __analizar_condicion(self):
        """
        Condición ::= Comparación ((divorcio|casorio) Comparación)?
        """
        # La primera sección obligatoria la comparación
        self.__analizar_comparacion()

        # Esta parte es opcional
        if self.componente_actual.tipo == TipoComponente.O:
            self.__avanzar_componente()
            self.__analizar_comparacion()
        elif self.componente_actual.tipo == TipoComponente.Y:
            self.__avanzar_componente()
            self.__analizar_comparacion()


    # Si no ha reventado vamos bien

    def __analizar_comparacion(self):
        """
        Comparación ::= Valor Comparador Valor
        """
        self.__analizar_valor()
        self.__verificar_comparador()
        self.__analizar_valor()

    def __analizar_valor(self):
        # El uno o el otro
        if self.componente_actual.tipo is TipoComponente.IDENTIFICADOR:
            self.__verificar_identificador()
        else:
            self.__analizar_literal()

    def __analizar_expresion_matematica(self):
        """
        ExpresiónMatemática ::= (Expresión) | Literal | Identificador
        """
        # Primera opción
        if self.componente_actual.texto == '(':
            self.__verificar('(')
            self.__analizar_expresion()
            self.__verificar(')')
        # Acá yo se que estan bien formados por que eso lo hizo el
        # explorador... es nada más revisar las posiciones.
        elif self.componente_actual.tipo in [TipoComponente.ENTERO, TipoComponente.FLOTANTE,
                                             TipoComponente.VALOR_VERDAD, TipoComponente.TEXTO]:
            self.__analizar_literal()

        # Este código se simplifica si invierto la opción anterior y esta
        else:
            self.__verificar_identificador()

    def __analizar_expresion(self):
        """
        Expresión ::= ExpresiónMatemática Operador ExpresiónMatemática
        """
        # Acá no hay nada que hacer todas son obligatorias en esas
        # posiciones
        self.__analizar_expresion_matematica()
        self.__verificar_operador()
        self.__analizar_expresion_matematica()

    def __analizar_literal(self):
        if self.componente_actual.tipo is TipoComponente.TEXTO:
            self.__verificar_texto()

        elif self.componente_actual.tipo is TipoComponente.VALOR_VERDAD:
            self.__analizar_valor_verdad()

        else:
            self.__analizar_numero()


    def __analizar_valor_verdad(self):
        self.__verificar_tipo(TipoComponente.VALOR_VERDAD)


    def __verificar_operador(self):

        self.__verificar_tipo(TipoComponente.OPERADOR_MATEMATICO)

    def __verificar_comparador(self):

        self.__verificar_tipo(TipoComponente.COMPARADOR)

    def __verificar_texto(self):

        self.__verificar_tipo(TipoComponente.TEXTO)

    def __analizar_numero(self):
        if self.componente_actual.tipo == TipoComponente.ENTERO:
            self.__verificar_entero()
        else:
            self.__verificar_flotante()

    def __verificar_entero(self):

        self.__verificar_tipo(TipoComponente.ENTERO)

    def __verificar_flotante(self):

        self.__verificar_tipo(TipoComponente.FLOTANTE)

    def __analizar_invocacion(self):

        # todos son obligatorios en ese orden
        self.__verificar_identificador()
        self.__verificar('(')
        self.__analizar_parametros()
        self.__verificar(')')

    def __verificar(self, texto_esperado):

        if self.componente_actual.texto != texto_esperado:
            # AQUI DEBE TIRAR UN ERROR! #
            print(self.componente_actual)
            print(texto_esperado)
            raise SyntaxError

        else:
            self.__avanzar_componente()

    def __verificar_identificador(self):

        self.__verificar_tipo(TipoComponente.IDENTIFICADOR)

    def __verificar_tipo(self, tipo_esperado):

        if self.componente_actual.tipo is not tipo_esperado:
            print(tipo_esperado)
            print(self.componente_actual)
            print("Error 2")
            raise SyntaxError(str(self.componente_actual) + " Error en componente")

        else:
            self.__avanzar_componente()

    def __avanzar_componente(self):

        self.pos_actual += 1
        if self.pos_actual < self.cantidad:
            self.componente_actual = self.lista_componentes[self.pos_actual]
            return
        return

    def __siguiente_componente(self, sig=1):

        return self.lista_componentes[self.pos_actual + sig]
