from explorador.ComponenteLéxico import ComponenteLéxico
from explorador.TipoComponente import TipoComponente
from utils.arbol import ArbolSintaxisAbstracta, NodoArbol, TipoNodo


class Analizador:

    componentes_lexicos : list
    cantidad_componentes: int
    posicion_componente_actual : int
    componente_actual : ComponenteLéxico

    def __init__(self, lista_componentes):

        self.componentes_lexicos = lista_componentes
        self.cantidad_componentes = len(lista_componentes)
        self.posicion_componente_actual = 0
        self.componente_actual = lista_componentes[0]
        self.asa = ArbolSintaxisAbstracta()

    def imprimir_asa(self):
        """
        Imprime el árbol de sintáxis abstracta
        """

        if self.asa.raiz is None:
            print([])
        else:
            self.asa.imprimir_preorden()

    def analizar(self):
        """
        Método principal que inicia el análisis siguiendo el esquema de
        análisis por descenso recursivo
        """
        self.asa.raiz = self.__analizar_programa()

    def __analizar_programa(self):
        """
        Programa ::= (Comentario | Asignación | Función)* Principal
        """

        nodos_nuevos = []

        # Los comentarios me los paso por el quinto forro del pantalón (y
        # de todos modos el Explorador ni siquiera los guarda como
        # componentes léxicos)

        # pueden venir múltiples asignaciones o funciones
        while (True):

            # Si es asignación
            if self.componente_actual.tipo == TipoComponente.IDENTIFICADOR:
                if self.__siguiente_componente().tipo == TipoComponente.ASIGNACION:
                    nodos_nuevos = [self.__analizar_asignacion()]
                elif self.__siguiente_componente().tipo == TipoComponente.FUNCION:
                    nodos_nuevos += [self.__analizar_funcion()]
            else:
                break

        return NodoArbol(TipoNodo.PROGRAMA, nodos=nodos_nuevos)


    def __analizar_asignacion(self):
        """
        Asignación ::= Identificador metale ( Literal | ExpresiónMatemática | (Invocación | Identificador) )

        Acá también cambié la gramática
        """

        nodos_nuevos = []

        # El identificador en esta posición es obligatorio
        nodos_nuevos += [self.__verificar_identificador()]

        # Igual el métale
        self.__verificar('->')


        # El siguiente bloque es de opcionales

        if self.componente_actual.tipo in [TipoComponente.ENTERO, TipoComponente.FLOTANTE, TipoComponente.VALOR_VERDAD, TipoComponente.TEXTO] :
            nodos_nuevos[0].setDict({'tipo_dato':self.componente_actual.tipo})
            nodos_nuevos += [self.__analizar_literal()]

        # los paréntesis obligatorios (es un poco feo)
        elif self.componente_actual.texto == '(':
            nodos_nuevos += [self.__analizar_expresion_matematica()]

        # Acá tengo que decidir si es Invocación o solo un identificador
        elif self.componente_actual.tipo == TipoComponente.IDENTIFICADOR:

            if self.__siguiente_componente().texto == '(':
                nodos_nuevos += [self.__analizar_invocacion()]
            else:

                nodos_nuevos += [self.__verificar_identificador()]

        else:
            raise SyntaxError('Viejo... acá algo reventó', self.componente_actual)

        return NodoArbol(TipoNodo.ASIGNACION, nodos=nodos_nuevos)

    def __analizar_expresion_matematica(self):
        """
        ExpresiónMatemática ::= (Expresión) | Número | Identificador
        """
        nodos_nuevos = []
        # Primera opción
        if self.componente_actual.texto == '(':

            # Los verificar no se incluyen por que son para forzar cierta
            # forma de escribir... pero no aportan nada a la semántica
            self.__verificar('(')

            nodos_nuevos += [self.__analizar_expresion()]

            self.__verificar(')')

        # Acá yo se que estan bien formados por que eso lo hizo el
        # explorador... es nada más revisar las posiciones.
        elif self.componente_actual.tipo == TipoComponente.ENTERO:
            nodos_nuevos += [self.__verificar_entero()]

        elif self.componente_actual.tipo == TipoComponente.FLOTANTE:
            nodos_nuevos += [self.__verificar_flotante()]

        elif self.componente_actual.tipo == TipoComponente.TEXTO:
            raise SyntaxError("Variables no es un numero")

        # Este código se simplifica si invierto la opción anterior y esta
        else:
            nodos_nuevos += [self.__verificar_identificador()]

        return NodoArbol(TipoNodo.EXPRESION_MATEMATICA, nodos=nodos_nuevos)

    def __analizar_expresion(self):
        """
        Expresión ::= ExpresiónMatemática Operador ExpresiónMatemática
        """

        nodos_nuevos = []

        # Acá no hay nada que hacer todas son obligatorias en esas
        # posiciones
        nodos_nuevos += [self.__analizar_expresion_matematica()]

        nodos_nuevos += [self.__verificar_operador()]

        nodos_nuevos += [self.__analizar_expresion_matematica()]

        return NodoArbol(TipoNodo.EXPRESION, nodos=nodos_nuevos)

    def __analizar_funcion(self):
        """
        Función ::= (Comentario)? mae Identificador (ParámetrosFunción) BloqueInstrucciones
        """

        nodos_nuevos = []
        # Comentario con doble check azul dela muerte... (ignorado)

        # Esta sección es obligatoria en este orden
        nodos_nuevos += [self.__verificar_identificador()]
        self.__verificar('-->')
        self.__verificar('(')
        nodos_nuevos += [self.__analizar_parametros_funcion()]
        self.__verificar(')')
        nodos_nuevos += [self.__analizar_bloque_instrucciones()]

        # La función lleva el nombre del identificador
        return NodoArbol(TipoNodo.FUNCION, contenido=nodos_nuevos[0].contenido, nodos=nodos_nuevos)

    def __analizar_invocacion(self):
        """
        Invocación ::= Identificador ( ParámetrosInvocación )
        """
        nodos_nuevos = []

        # todos son obligatorios en ese orden
        nodos_nuevos += [self.__verificar_identificador()]
        self.__verificar('(')
        nodos_nuevos += [self.__analizar_parametros_invocacion()]
        self.__verificar(')')

        return NodoArbol(TipoNodo.INVOCACION, nodos=nodos_nuevos)

    def __analizar_parametros_invocacion(self):
        """
        ParametrosInvocación ::= Valor (/ Valor)+
        """
        nodos_nuevos = []

        # Fijo un valor tiene que haber
        nodos_nuevos += [self.__analizar_valor()]

        while (self.componente_actual.texto == ','):
            self.__verificar(',')
            nodos_nuevos += [self.__analizar_valor()]

        # Esto funciona con lógica al verrís... Si no revienta con error

        return NodoArbol(TipoNodo.PARAMETROS_INVOCACION, nodos=nodos_nuevos)

    def __analizar_parametros_funcion(self):
        """
        ParametrosFunción ::= Identificador (/ Identificador)+
        """
        nodos_nuevos = []

        # Fijo un valor tiene que haber
        nodos_nuevos += [self.__verificar_identificador()]

        while (self.componente_actual.texto == ','):
            self.__verificar(',')
            nodos_nuevos += [self.__verificar_identificador()]

        # Esto funciona con lógica al verrís... Si no revienta con error

        return NodoArbol(TipoNodo.PARAMETROS_FUNCION, nodos=nodos_nuevos)

    def __analizar_instruccion(self):
        nodos_nuevos = []
        if self.componente_actual.texto == '^':
            nodos_nuevos += [self.__analizar_repeticion()]
        elif self.componente_actual.texto == '?':
            nodos_nuevos += [self.__bifu_si()]
        elif self.componente_actual.texto == '?:':
            nodos_nuevos += [self.__bifu_TalVez()]
        elif self.componente_actual.texto == ':':
            nodos_nuevos += [self.__SiNo()]
        # elif self.componente_actual.texto == '++':
        #     nodos_nuevos += [self.__concatenar()]
        elif self.componente_actual.tipo == TipoComponente.IDENTIFICADOR:

            if self.__siguiente_componente().texto == '->':
                nodos_nuevos += [self.__analizar_asignacion()]
            else:
                nodos_nuevos += [self.__analizar_invocacion()]

        elif self.componente_actual.texto == '>>':
            nodos_nuevos += [self.__analizar_retorno()]

        else:  # Muy apropiado el chiste de ir a revisar si tiene error al último.
            nodos_nuevos += [self.__analizar_error()]

        # Ignorado el comentario

        # Acá yo debería volarme el nivel Intrucción por que no aporta nada
        return NodoArbol(TipoNodo.INSTRUCCION, nodos=nodos_nuevos)

    def __analizar_repeticion(self):
        """
        Repetición ::= upee ( Condición ) BloqueInstrucciones
        """
        nodos_nuevos = []

        # Todos presentes en ese orden... sin opciones
        self.__verificar('^')
        self.__verificar('(')
        nodos_nuevos += [self.__analizar_condicion()]
        self.__verificar(')')

        # Yo acá tengo dos elecciones... creo otro nivel con Bloque de
        # instrucciones o pongo directamente las instrucciones en este
        # nivel... yo voy con la primera por facilidad... pero eso hace más
        # grande el árbol
        nodos_nuevos += [self.__analizar_bloque_instrucciones()]

        return NodoArbol(TipoNodo.REPETICION, nodos=nodos_nuevos)

    def __bifu_si(self):
        nodos_nuevos = []

        # Todos presentes en ese orden... sin opciones
        self.__verificar('?')
        self.__verificar('(')
        nodos_nuevos += [self.__analizar_condicion()]
        self.__verificar(')')

        nodos_nuevos += [self.__analizar_bloque_instrucciones()]

        return NodoArbol(TipoNodo.SI, nodos=nodos_nuevos)


    def __bifu_TalVez(self):
        nodos_nuevos = []

        # Todos presentes en ese orden... sin opciones
        self.__verificar('?:')
        self.__verificar('(')
        nodos_nuevos += [self.__analizar_condicion()]
        self.__verificar(')')

        nodos_nuevos += [self.__analizar_bloque_instrucciones()]

        return NodoArbol(TipoNodo.TALVEZ, nodos=nodos_nuevos)

    def __SiNo(self):

        nodos_nuevos = []

        # Todos presentes en ese orden... sin opciones
        self.__verificar(':')
        nodos_nuevos += [self.__analizar_bloque_instrucciones()]

        return NodoArbol(TipoNodo.SINO, nodos=nodos_nuevos)

    def __analizar_condicion(self):
        """
        Condición ::= Comparación ((divorcio|casorio) Comparación)?
        """
        nodos_nuevos = []

        # La primera sección obligatoria la comparación
        nodos_nuevos += [self.__analizar_comparacion()]

        if self.componente_actual.tipo == TipoComponente.Y :
                nodo = NodoArbol(TipoNodo.OPERADOR_LOGICO, contenido='&&')
                nodos_nuevos += [nodo]
                self.__verificar('&&')
                nodos_nuevos += [self.__analizar_comparacion()]


        elif self.componente_actual.tipo == TipoComponente.O :
                nodo = NodoArbol(TipoNodo.OPERADOR_LOGICO, contenido='||')
                nodos_nuevos += [nodo]
                self.__verificar('||')
                nodos_nuevos += [self.__analizar_comparacion()]

            # Un poco tieso, pero funcional
        

        # Si no ha reventado vamos bien
        return NodoArbol(TipoNodo.CONDICION, nodos=nodos_nuevos)

    def __analizar_comparacion(self):

        nodos_nuevos = []

        nodos_nuevos += [self.__analizar_valor()]
        nodos_nuevos += [self.__verificar_comparador()]
        nodos_nuevos += [self.__analizar_valor()]

        return NodoArbol(TipoNodo.COMPARACION, nodos=nodos_nuevos)

    def __analizar_valor(self):

        if self.componente_actual.tipo is TipoComponente.IDENTIFICADOR:
            nodo = self.__verificar_identificador()
        else:
            nodo = self.__analizar_literal()

        return nodo

    def __analizar_retorno(self):
        """
        Retorno :: sarpe (Valor)?
        """
        nodos_nuevos = []

        self.__verificar('>>')

        # Este hay que validarlo para evitar el error en caso de que no
        # aparezca
        if self.componente_actual.tipo in [TipoComponente.IDENTIFICADOR, TipoComponente.ENTERO, TipoComponente.FLOTANTE,
                                           TipoComponente.VALOR_VERDAD, TipoComponente.TEXTO]:
            nodos_nuevos += [self.__analizar_valor()]


        return NodoArbol(TipoNodo.RETORNO, nodos=nodos_nuevos)

    def __analizar_error(self):

        nodos_nuevos = []

        if self.componente_actual.tipo == TipoComponente.ERRORVARIABLE:
            print(self.componente_actual)
            self.__avanzar_componente()
            nodos_nuevos += [self.__analizar_valor()]
            raise SyntaxError(str(self.componente_actual) + '  Error a la hora de definir variable')
        elif self.componente_actual.tipo == TipoComponente.ERRORFLOTANTE:
            self.__avanzar_componente()
            nodos_nuevos += [self.__analizar_valor()]
            raise SyntaxError(str(self.componente_actual) + '  Error a la hora de definir Numeros Flotantes')
        else:
            self.__avanzar_componente()
            nodos_nuevos += [self.__analizar_valor()]
            raise SyntaxError(str(self.componente_actual) + ' Error en algun lugar')

        return NodoArbol(TipoNodo.ERROR, nodos=nodos_nuevos)


    def __analizar_literal(self):
        """
        Literal ::= (Número | Texto | ValorVerdad)
        """

        # Acá le voy a dar vuelta por que me da pereza tanta validación
        if self.componente_actual.tipo is TipoComponente.TEXTO:
            nodo = self.__verificar_texto()

        elif self.componente_actual.tipo is TipoComponente.VALOR_VERDAD:
            nodo = self.__verificar_valor_verdad()

        else:
            nodo = self.__analizar_numero()

        return nodo

    def __analizar_numero(self):
        """
        Número ::= (Entero | Flotante)
        """
        if self.componente_actual.tipo == TipoComponente.ENTERO:
            nodo = self.__verificar_entero()
        else:
            nodo = self.__verificar_flotante()

        return nodo

    def __analizar_bloque_instrucciones(self):
        """
        Este es nuevo y me lo inventé para simplicicar un poco el código...
        correspondería actualizar la gramática.

        BloqueInstrucciones ::= { Instrucción+ }
        """
        nodos_nuevos = []

        # Obligatorio
        self.__verificar('{')

        # mínimo una
        nodos_nuevos += [self.__analizar_instruccion()]

        # Acá todo puede venir uno o más
        while self.componente_actual.texto in ['^', '?', '?:', ':', '>>', '++'] \
                or self.componente_actual.tipo == TipoComponente.IDENTIFICADOR:
            nodos_nuevos += [self.__analizar_instruccion()]

        # Obligatorio
        self.__verificar('}')

        return NodoArbol(TipoNodo.BLOQUE_INSTRUCCIONES, nodos=nodos_nuevos)

    def __verificar_operador(self):
        """
        Operador ::= (echele | quitele | chuncherequee | desmadeje)
        """
        self.__verificar_tipo_componente(TipoComponente.OPERADOR_MATEMATICO)

        nodo = NodoArbol(TipoNodo.OPERADOR, contenido=self.componente_actual.texto)
        self.__avanzar_componente()

        return nodo

    def __verificar_valor_verdad(self):
        """
        ValorVerdad ::= (True | False)
        """
        self.__verificar_tipo_componente(TipoComponente.VALOR_VERDAD)

        nodo = NodoArbol(TipoNodo.VALOR_VERDAD, contenido=self.componente_actual.texto)
        self.__avanzar_componente()
        return nodo

    def __verificar_comparador(self):

        self.__verificar_tipo_componente(TipoComponente.COMPARADOR)

        nodo = NodoArbol(TipoNodo.COMPARADOR, contenido=self.componente_actual.texto)
        self.__avanzar_componente()
        return nodo

    def __verificar_texto(self):

        self.__verificar_tipo_componente(TipoComponente.TEXTO)

        nodo = NodoArbol(TipoNodo.TEXTO, contenido=self.componente_actual.texto)
        self.__avanzar_componente()
        return nodo

    def __verificar_entero(self):
        """
        Verifica si el tipo del componente léxico actuales de tipo ENTERO

        Entero ::= (-)?\d+
        """
        self.__verificar_tipo_componente(TipoComponente.ENTERO)

        nodo = NodoArbol(TipoNodo.ENTERO, contenido=self.componente_actual.texto)
        self.__avanzar_componente()
        return nodo

    def __verificar_flotante(self):
        """
        Verifica si el tipo del componente léxico actuales de tipo FLOTANTE

        Flotante ::= (-)?\d+.(-)?\d+
        """
        self.__verificar_tipo_componente(TipoComponente.FLOTANTE)

        nodo = NodoArbol(TipoNodo.FLOTANTE, contenido=self.componente_actual.texto)
        self.__avanzar_componente()
        return nodo

    def __verificar_identificador(self):
        """
        Verifica si el tipo del componente léxico actuales de tipo
        IDENTIFICADOR

        Identificador ::= [a-z][a-zA-Z0-9]+
        """
        self.__verificar_tipo_componente(TipoComponente.IDENTIFICADOR)

        nodo = NodoArbol(TipoNodo.IDENTIFICADOR, contenido=self.componente_actual.texto)
        self.__avanzar_componente()
        return nodo

    def __componentex2(self, sig=2):

        return self.componentes_lexicos[self.posicion_componente_actual + sig]

    def __verificar(self, texto_esperado):

        """
        Verifica si el texto del componente léxico actual corresponde con
        el esperado cómo argumento
        """
        
        if self.componente_actual.texto != texto_esperado:
            print()
            print('Componente actual: ', self.componente_actual.texto)
            print('Texto esperado: ', texto_esperado)
            raise SyntaxError((texto_esperado, str(self.componente_actual)))

        self.__avanzar_componente()


    def __verificar_tipo_componente(self, tipo_esperado):

        if self.componente_actual.tipo is not tipo_esperado:
            print(tipo_esperado)
            print(self.componente_actual)
            print("Error 2")
            raise SyntaxError(str(self.componente_actual) + " Error en componente")

        """
        else:
            self.__avanzar_componente()
        """

    def __avanzar_componente(self):

        self.posicion_componente_actual += 1
        if self.posicion_componente_actual < self.cantidad_componentes:
            self.componente_actual = self.componentes_lexicos[self.posicion_componente_actual]
            return
        return

    def __siguiente_componente(self, sig=1):

        return self.componentes_lexicos[self.posicion_componente_actual + sig]





