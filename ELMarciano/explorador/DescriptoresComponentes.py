from explorador.TipoComponente import TipoComponente


class DescriptoresComponentes:
    # contiene una lista de tuplas de CATEGORIA - Regex.
    descriptores_componentes = [
        (TipoComponente.COMENTARIO, r'^//.*'),
        (TipoComponente.PUNTUACION, r'^([(),])'),
        (TipoComponente.PALABRA_CLAVE, r'^({|}|>>)'),
        (TipoComponente.ASIGNACION, r'^(->)'),
        (TipoComponente.FUNCION, r'^(-->)'),
        (TipoComponente.CONDICIONAL, r'^(\?:|\?|:)'),
        (TipoComponente.REPETICION, r'^(\^)'),
        (TipoComponente.FLOTANTE, r'^-?\d+\.\d+'),
        (TipoComponente.ENTERO, r'^(-)?\d+'),
        (TipoComponente.AMBIENTE_ESTANDAR, r'^(_|\+\+)'),
        (TipoComponente.OPERADOR_MATEMATICO, r'^(\+|-|\*|/)'),
        (TipoComponente.COMPARADOR, r'^(=|!=|<|>|<=|>=)'),
        (TipoComponente.VALOR_VERDAD, r'^(T|F)'),
        (TipoComponente.IDENTIFICADOR, r'^([a-z]([a-zA-Z0-9])*)'),
        (TipoComponente.ERROR_ENCAPSULACION, r'^((})[a-zA-Z- _/])'),
        (TipoComponente.ERRORFLOTANTE, r'^(\s\.[0-9])'),
        (TipoComponente.ESPACIO, r'^(\s)+'),
        (TipoComponente.O, r'^(\|\|)'),
        (TipoComponente.Y, r'^(&&)'),
        (TipoComponente.ERRORVARIABLE, r'^([:{}~!@%&|;?]+)'),
        (TipoComponente.TEXTO, r'^".*"'),
        (TipoComponente.ERROR, r'^(\s)*')
    ]
