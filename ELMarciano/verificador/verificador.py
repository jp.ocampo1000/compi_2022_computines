from utils.arbol import ArbolSintaxisAbstracta, NodoArbol, TipoNodo
from utils.tipo_datos import TipoDatos
from verificador.Visitante import Visitante
from verificador.TablaSimbolos import TablaSimbolos
class Verificador:
    asa: ArbolSintaxisAbstracta
    visitador: Visitante
    tabla_simbolos: TablaSimbolos

    def __init__(self, nuevo_asa: ArbolSintaxisAbstracta):
        self.asa = nuevo_asa

        self.tabla_simbolos = TablaSimbolos()
        ##self.__cargar_ambiente_estándar()

        self.visitador = Visitante(self.tabla_simbolos)


    def imprimirTabla(self):
        self.tabla_simbolos.limpiarRegistros()
        print(self.tabla_simbolos.__str__())

    def imprimir_asa(self):
        """
        Imprime el árbol de sintáxis abstracta
        """

        if self.asa.raiz is None:
            print([])
        else:
            self.asa.imprimir_preorden()

    def __cargar_ambiente_estandar(self):
        ###Muestre|Ingrese|Mezclar_ingredientes|Mimir
        funciones_estandar = [('>>', TipoDatos.NINGUNO),
                              ('Ingrese', TipoDatos.TEXTO),
                              ('Mimir', TipoDatos.NINGUNO),
                              ('Mezclar_ingredientes', TipoDatos.TEXTO)]

        for nombre, tipo in funciones_estandar:
            nodo = NodoArbol(TipoNodo.FUNCION, contenido=nombre, atributos={'tipo': tipo})
            self.tabla_simbolos.nuevo_registro(nodo)

    def verificar(self):
        self.visitador.visitar(self.asa.raiz)

    def tiene_errores(self):
        if self.tabla_simbolos.lista_errores != []:
            return True
        return False

    def imprimir_errores(self):
        '''
        Funcion para imprimir los errores que detectó el verificador
        '''
        print('\n', 'Lista de Errores Verificador:')
        for error in self.tabla_simbolos.lista_errores:
            print('\n', 'Error #' + str(self.tabla_simbolos.lista_errores.index(error) + 1))
            print(error)
        print('\n', 'Fin de lista de Errores')

