from utils.arbol import TipoNodo, NodoArbol
from utils.tipo_datos import TipoDatos
from verificador import TablaSimbolos
from explorador.TipoComponente import TipoComponente


class Visitante:


    tabla_simbolos: TablaSimbolos

    def __init__(self, nueva_tabla_simbolos):
        self.tabla_simbolos = nueva_tabla_simbolos


    def visitar(self, nodo: NodoArbol):

        if nodo.tipo is TipoNodo.PROGRAMA:
            self.__visitar_programa(nodo)

        elif nodo.tipo is TipoNodo.ASIGNACION:
            self.__visitar_asignacion(nodo)

        elif nodo.tipo is TipoNodo.EXPRESION_MATEMATICA:
            self.__visitar_expresion_matematica(nodo)

        elif nodo.tipo is TipoNodo.EXPRESION:
            self.__visitar_expresion(nodo)

        elif nodo.tipo is TipoNodo.FUNCION:
            self.__visitar_funcion(nodo)

        elif nodo.tipo is TipoNodo.INVOCACION:
            self.__visitar_invocacion(nodo)

        elif nodo.tipo is TipoNodo.PARAMETROS_INVOCACION:
            self.__visitar_parametros_invocacion(nodo)

        elif nodo.tipo is TipoNodo.PARAMETROS_FUNCION:
            self.__visitar_parametros_funcion(nodo)

        elif nodo.tipo is TipoNodo.INSTRUCCION:
            self.__visitar_instruccion(nodo)

        elif nodo.tipo is TipoNodo.REPETICION:
            self.__visitar_repeticion(nodo)

        elif nodo.tipo is TipoNodo.SI:
            self.__visitar_si(nodo)

        elif nodo.tipo is TipoNodo.TALVEZ:
            self.__visitar_talvez(nodo)

        elif nodo.tipo is TipoNodo.SINO:
            self.__visitar_sino(nodo)

        elif nodo.tipo is TipoNodo.OPERADOR_LOGICO:
            self.__visitar_operador_logico(nodo)

        elif nodo.tipo is TipoNodo.CONDICION:
            self.__visitar_condicion(nodo)

        elif nodo.tipo is TipoNodo.COMPARACION:
            self.__visitar_comparacion(nodo)

        elif nodo.tipo is TipoNodo.RETORNO:
            self.__visitar_retorno(nodo)

        elif nodo.tipo is TipoNodo.BLOQUE_INSTRUCCIONES:
            self.__visitar_bloque_instrucciones(nodo)

        elif nodo.tipo is TipoNodo.OPERADOR:
            self.__visitar_operador(nodo)

        elif nodo.tipo is TipoNodo.VALOR_VERDAD:
            self.__visitar_veracidad(nodo)

        elif nodo.tipo is TipoNodo.COMPARADOR:
            self.__visitar_comparador(nodo)

        elif nodo.tipo is TipoNodo.TEXTO:
            self.__visitar_texto(nodo)

        elif nodo.tipo is TipoNodo.ENTERO:
            self.__visitar_entero(nodo)

        elif nodo.tipo is TipoNodo.FLOTANTE:
            self.__visitar_flotante(nodo)

        elif nodo.tipo is TipoNodo.IDENTIFICADOR:
            self.__visitar_identificador(nodo)

        else:
            # Puse esta opción nada más para que se vea bonito...
            raise Exception('Error no deberia llegar aqui')



    def manejar_errores(self, nodo, tipoError):
        tipo = nodo.tipo
        componente = nodo.contenido
        if(not self.tabla_simbolos.recuperandose_error):
            if tipoError == TipoNodo.INVOCACION:
                texto = f'<Error: El valor {componente.texto} en la fila {componente.atributos_adicionales.fila}, en la columna {componente.atributos_adicionales.columna} deberia ser de tipo funcion pero es de tipo {tipo.name}'
            elif tipoError == TipoNodo.PARAMETROS_INVOCACION:
                texto =  f'<Error: El valor {componente.texto} en la fila {componente.atributos_adicionales.fila}, en la columna {componente.atributos_adicionales.columna} deberia ser de una variable pero es de tipo {tipo.name}'

            self.tabla_simbolos.lista_errores.append(texto)
            self.tabla_simbolos.recuperandose_error = True
            print(texto)


    def __visitar_programa(self, nodo_actual:NodoArbol):

        for nodo in nodo_actual.nodos:
            if self.tabla_simbolos.recuperandose_error:
                self.tabla_simbolos.recuperandose_error = False
            nodo.visitar(self)



    def __visitar_asignacion(self, nodo_actual:NodoArbol):
        # Metó la información en la tabla de símbolos (IDENTIFICACIÓN)
        self.tabla_simbolos.nuevo_registro_de_asignacion(nodo_actual.nodos[0], nodo_actual.nodos[0].atributos.get('tipo_dato'))


        for nodo in nodo_actual.nodos:

            # Verifico que exista si es un identificador (IDENTIFICACIÓN)
            if nodo.tipo == TipoNodo.IDENTIFICADOR:

                registro = self.tabla_simbolos.verificar_existencia(nodo.contenido)
            nodo.visitar(self)

        # Si es una función verifico el tipo que retorna para incluirlo en
        # la asignación y si es un literal puedo anotar el tipo (TIPO)

        nodo_actual.atributos['tipo'] = nodo_actual.nodos[1].atributos['tipo']

        nodo_actual.nodos[0].atributos['tipo'] = nodo_actual.nodos[1].atributos['tipo']



    def __visitar_expresion_matematica(self, nodo_actual:NodoArbol):

        for nodo in nodo_actual.nodos:
            # Verifico que exista si es un identificador (IDENTIFICACIÓN)

            if nodo.tipo == TipoNodo.IDENTIFICADOR:
                if not self.tabla_simbolos.verificar_existencia_de_vari(str(nodo.contenido)):
                    raise SyntaxError("La variable no esta definida")

                if self.tabla_simbolos.validar_tipo_dato(str(nodo.contenido)) != TipoComponente.ENTERO and self.tabla_simbolos.validar_tipo_dato(str(nodo.contenido)) != TipoComponente.FLOTANTE and self.tabla_simbolos.validar_tipo_dato(str(nodo.contenido)) != None:
                    raise SyntaxError("Los Tipos de Datos no son Compatibles en la operacion")

            nodo.visitar(self)

        # Anoto el tipo de datos 'NÚMERO' (TIPO)
        nodo_actual.atributos['tipo'] = TipoDatos.NUMERO



    
    def __visitar_operador_logico(self, nodo_actual:NodoArbol):
        for nodo in nodo_actual.nodos:
            nodo.visitar(self)
    

    def __visitar_expresion(self, nodo_actual: NodoArbol):

        for nodo in nodo_actual.nodos:
            nodo.visitar(self)

        # Anoto el tipo de datos 'NÚMERO' (TIPO)
        nodo_actual.atributos['tipo'] = TipoDatos.NUMERO

    def __visitar_funcion(self, nodo_actual: NodoArbol):


        # Meto la función en la tabla de símbolos (IDENTIFICACIÓN)
        self.tabla_simbolos.nuevo_registro(nodo_actual)

        self.tabla_simbolos.abrir_bloque()

        for nodo in nodo_actual.nodos:
            # print(nodo)
            nodo.visitar(self)

        self.tabla_simbolos.cerrar_bloque()

        # Anoto el tipo de retorno (TIPO)
        nodo_actual.atributos['tipo'] = nodo_actual.nodos[-1].atributos['tipo']


    def __visitar_invocacion(self, nodo_actual):

        # Verfica que el 'Identificador' exista (IDENTIFICACIÓN) y que sea
        registro = self.tabla_simbolos.verificar_existencia(nodo_actual.nodos[0].contenido)

        if registro['referencia'].tipo != TipoNodo.FUNCION:
            self.manejar_errores(registro['referencia'],TipoNodo.INVOCACION)
            #raise Exception('Esa vara es una variable...', registro)

        for nodo in nodo_actual.nodos:
            nodo.visitar(self)

        # El tipo resultado de la invocación es el tipo inferido de una
        # función previamente definida
        nodo_actual.atributos['tipo'] = registro['referencia'].atributos['tipo']



    def __visitar_parametros_invocacion(self, nodo_actual):
        # Recordemos que 'Valor' no existe en el árbol...

        # Si es 'Identificador' verifico que exista (IDENTIFICACIÓN)
        for nodo in nodo_actual.nodos:
            # Si existe y no es función ya viene con el tipo por que
            # fue producto de una asignación
            if nodo.tipo == TipoNodo.IDENTIFICADOR:
                registro = self.tabla_simbolos.verificar_existencia(nodo.contenido)

            elif nodo.tipo == TipoNodo.FUNCION:
                self.manejar_errores(registro['referencia'],TipoNodo.PARAMETROS_INVOCACION)
                #raise Exception('Esa vara es una función...', nodo.contenido)

            # Si es número o texto nada más los visito
            nodo.visitar(self)

        # No hay tipos en los parámetros... se sabe en tiempo de ejecución

    def __visitar_parametros_funcion(self, nodo_actual):
        #print(nodo_actual)
        # Registro cada 'Identificador' en la tabla
        for nodo in nodo_actual.nodos:
            #print(nodo)
            #print(nodo.contenido.texto)
            self.tabla_simbolos.nuevo_registro(nodo)
            nodo.visitar(self)



    def __visitar_instruccion(self, nodo_actual):
        # Por alguna razón no me volé este nivel.. así que lo visitamos...
        # Esto es un desperdicio de memoria y de cpu

        # Visita la instrucción

        # Lo pongo así por copy/paste... pero puede ser como el comentario
        # de más abajo.
        for nodo in nodo_actual.nodos:
            nodo.visitar(self)
            nodo_actual.atributos['tipo'] = nodo.atributos['tipo']

        # nodo_actual.nodos[0].visitar(self)


    def __visitar_repeticion(self, nodo_actual):
        # Visita la condición


        # Visita el bloque de instrucciones

        # Lo pongo así por copy/paste... pero puede ser como el comentario
        # de más abajo.
        self.tabla_simbolos.abrir_bloque()

        for nodo in nodo_actual.nodos:
            nodo.visitar(self)

        # nodo_actual.nodos[0].visitar(self)

        self.tabla_simbolos.cerrar_bloque()

        # Anoto el tipo de retorno (TIPO)
        nodo_actual.atributos['tipo'] = nodo_actual.nodos[1].atributos['tipo']


    def __visitar_si(self, nodo_actual):
        # Visita el bloque de instrucciones

        # Lo pongo así por copy/paste... pero puede ser como el comentario
        # de más abajo.
        self.tabla_simbolos.abrir_bloque()

        for nodo in nodo_actual.nodos:
            nodo.visitar(self)

        # nodo_actual.nodos[0].visitar(self)

        self.tabla_simbolos.cerrar_bloque()

        # Anoto el tipo de retorno (TIPO)
        nodo_actual.atributos['tipo'] = nodo_actual.nodos[1].atributos['tipo']


    def __visitar_talvez(self, nodo_actual):
        # Visita la condición
        # Visita el bloque de instrucciones

        # Lo pongo así por copy/paste... pero puede ser como el comentario
        # de más abajo.
        self.tabla_simbolos.abrir_bloque()

        for nodo in nodo_actual.nodos:
            nodo.visitar(self)

        # nodo_actual.nodos[0].visitar(self)

        self.tabla_simbolos.cerrar_bloque()

        # Anoto el tipo de retorno (TIPO)
        nodo_actual.atributos['tipo'] = nodo_actual.nodos[1].atributos['tipo']



    def __visitar_sino(self, nodo_actual):
        # Visita el bloque de instrucciones

        # Lo pongo así por copy/paste... pero puede ser como el comentario
        # de más abajo.
        self.tabla_simbolos.abrir_bloque()

        for nodo in nodo_actual.nodos:
            nodo.visitar(self)

        # nodo_actual.nodos[0].visitar(self)

        self.tabla_simbolos.cerrar_bloque()

        # Anoto el tipo de retorno (TIPO)
        nodo_actual.atributos['tipo'] = nodo_actual.nodos[0].atributos['tipo']


    def __visitar_condicion(self, nodo_actual):

        for nodo in nodo_actual.nodos:
            nodo.visitar(self)

        # Comparación retorna un valor de verdad (TIPO)
        nodo_actual.atributos['tipo'] = TipoDatos.VALOR_VERDAD



    def __visitar_comparacion(self, nodo_actual):

        # Si los 'Valor' son identificadores se asegura que existan (IDENTIFICACIÓN)
        for nodo in nodo_actual.nodos:
            if nodo.tipo == TipoNodo.IDENTIFICADOR:
                registro = self.tabla_simbolos.verificar_existencia(nodo.contenido)

            nodo.visitar(self)


        # Verifico que los tipos coincidan (TIPO)
        valor_izq      = nodo_actual.nodos[0]
        comparador  = nodo_actual.nodos[1]
        valor_der      = nodo_actual.nodos[2]
        # Ya se que eso se ve sueltelefeo... pero ya el cerebro se me apagó...

        if valor_izq.atributos['tipo'] == valor_der.atributos['tipo']:
            comparador.atributos['tipo'] = valor_izq.atributos['tipo']

            # Una comparación siempre tiene un valor de verdad
            nodo_actual.atributos['tipo'] = TipoDatos.VALOR_VERDAD

        # Caso especial loco: Si alguno de los dos es un identificador de
        # un parámetro de función no puedo saber que tipo tiene o va a
        # tener por que este lenguaje no es tipado... tons vamos a poner
        # que la comparación puede ser cualquiera
        elif valor_izq.atributos['tipo'] == TipoDatos.CUALQUIERA or \
                valor_der.atributos['tipo'] == TipoDatos.CUALQUIERA:

            comparador.atributos['tipo'] = TipoDatos.CUALQUIERA

            # Todavía no estoy seguro.
            nodo_actual.atributos['tipo'] = TipoDatos.CUALQUIERA

        else:
            componente = nodo_actual.contenido
            texto = '<Error: El valor '+componente.texto+' en la fila '+str(componente.atributos_adicionales.fila)+', en la columna ' +str(componente.atributos_adicionales.columna)+' hubo un error en la comparación'
            self.tabla_simbolos.lista_errores.append(texto)
            self.tabla_simbolos.recuperandose_error = True
            #raise Exception('Papo, algo tronó acá', str(nodo_actual))


    def __visitar_valor(self, nodo_actual):
        """
        --Valor ::= (Identificador | Literal)
        ++Valor ::= (Identificador | Literal)

        """
        # En realidad núnca se va a visitar por que lo saqué del árbol
        # duránte la etapa de análisiss

    def __visitar_retorno(self, nodo_actual):


        for nodo in nodo_actual.nodos:
            nodo.visitar(self)

        if nodo_actual.nodos == []:
            # Si no retorna un valor no retorna un tipo específico
            nodo_actual.atributos['tipo'] = TipoDatos.NINGUNO

        else:

            for nodo in nodo_actual.nodos:

                nodo.visitar(self)

                if nodo.tipo == TipoNodo.IDENTIFICADOR:
                    # Verifico si valor es un identificador que exista (IDENTIFICACIÓN)
                    registro = self.tabla_simbolos.verificar_existencia(nodo.contenido)
                    if registro != None:
                        # le doy al sarpe el tipo de retorno del identificador encontrado
                        nodo_actual.atributos['tipo'] = registro['referencia'].atributos['tipo']

                else:
                    # Verifico si es un Literal de que tipo es (TIPO)
                    nodo_actual.atributos['tipo'] = nodo.atributos['tipo']




    def __visitar_bloque_instrucciones(self, nodo_actual):

        # Visita todas las instrucciones que contiene
        for nodo in nodo_actual.nodos:
            nodo.visitar(self)

        # Acá yo debería agarrar el tipo de datos del Retorno si lo hay
        nodo_actual.atributos['tipo'] = TipoDatos.NINGUNO

        for nodo in nodo_actual.nodos:
            if nodo.atributos['tipo'] != TipoDatos.NINGUNO:
                nodo_actual.atributos['tipo'] = nodo.atributos['tipo']



    def __visitar_operador(self, nodo_actual):
        # Operador para trabajar con números (TIPO)
        nodo_actual.atributos['tipo'] = TipoDatos.NUMERO

    def __visitar_veracidad(self, nodo_actual):
        # Valor de verdad (TIPO)
        nodo_actual.atributos['tipo'] = TipoDatos.VALOR_VERDAD

    def __visitar_comparador(self, nodo_actual):
        # Estos comparadores son numéricos  (TIPO)
        # (cañazo | poquitico | misma vara | otra vara | menos o igualitico | más o igualitico)
        if nodo_actual.contenido not in ['misma vara', 'otra vara' ]:
            nodo_actual.atributos['tipo'] = TipoDatos.NUMERO

        else:
            nodo_actual.atributos['tipo'] = TipoDatos.CUALQUIERA
            # Si no es alguno de esos puede ser Numérico o texto y no lo puedo
            # inferir todavía


    def __visitar_texto(self, nodo_actual):
        # Texto (TIPO)
        nodo_actual.atributos['tipo'] = TipoDatos.TEXTO

    def __visitar_entero(self, nodo_actual):
        # Entero (TIPO)
        nodo_actual.atributos['tipo'] = TipoDatos.NUMERO

    def __visitar_flotante(self, nodo_actual):
        # Flotante (TIPO)
        nodo_actual.atributos['tipo'] = TipoDatos.NUMERO

    def __visitar_identificador(self, nodo_actual):
        nodo_actual.atributos['tipo'] = TipoDatos.CUALQUIERA


