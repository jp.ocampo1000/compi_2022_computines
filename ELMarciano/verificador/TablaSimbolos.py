class TablaSimbolos:

    simbolos: list
    profundidad: int
    recuperandose_error: bool = False
    lista_errores: list = []

    def __init__(self):
        self.simbolos = []
        self.profundidad = 0

    def manejar_errores(self, componente):
        if(not self.recuperandose_error):
            self.lista_errores.append(f'<Error: El valor {componente.texto} en la fila {componente.atributos_adicionales.fila}, en la columna {componente.atributos_adicionales.columna} no se encuentra declarado.')
            self.recuperandose_error = True


    def abrir_bloque(self):
        self.profundidad += 1


    def imprimirSimbolos(self):
        print(self.simbolos)

    def cerrar_bloque(self):
        """
        Termina un bloque de alcance y al acerlo elimina todos los
        registros de la tabla que estan en ese bloque
        """

        for registro in self.simbolos:
            if registro['profundidad'] == self.profundidad:
                #self.simbolos.remove(registro)
                print()

        self.profundidad -= 1

    def validar_tipo_dato(self, componente):
        nombre = componente
        for registro in self.simbolos:
            #print(registro['nombre'])
            #print(nombre)
            # si existe
            if registro['nombre'] == nombre:
                return registro['tipo_dato']

        return None

    def nuevo_registro_de_asignacion(self, nodo, tipo_dato):
        diccionario = {}

        diccionario['nombre'] = nodo.contenido
        diccionario['profundidad'] = self.profundidad
        diccionario['referencia'] = nodo
        diccionario['tipo_dato'] = tipo_dato


        self.simbolos.append(diccionario)

    def nuevo_registro(self, nodo):
        diccionario = {}

        diccionario['nombre'] = nodo.contenido
        diccionario['profundidad'] = self.profundidad
        diccionario['referencia'] = nodo
        diccionario['tipo_dato'] = None
        self.simbolos.append(diccionario)


    def limpiarRegistros(self):
        temp = self.simbolos
        elements = []

        for i in temp:
            for j in self.simbolos:
                if i['nombre'] == j['nombre'] and i['profundidad'] > j['profundidad']:
                    elements.append(i)
        for k in elements:
            self.simbolos.remove(k)


    def verificar_existencia(self, componente):

        nombre = componente
        for registro in self.simbolos:
            #print(registro['nombre'])
            #print(nombre)
            # si existe
            if registro['nombre'] == nombre:
                return registro

        return None

    def verificar_existencia_de_vari(self, componente):

        nombre = componente
        for registro in self.simbolos:
            #print(registro['nombre'])
            #print(nombre)
            # si existe
            if registro['nombre'] == nombre:
                return True

        return False



    def __str__(self):

        resultado = 'TABLA DE SÍMBOLOS\n\n'
        resultado += 'Profundidad: ' + str(self.profundidad) +'\n\n'
        for registro in self.simbolos:
            resultado += str(registro) + '\n'

        return resultado