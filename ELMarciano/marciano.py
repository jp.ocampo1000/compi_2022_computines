#Compilador para El Marciano

# - - - [ Imports ] - - -
from analizador.analizador_gramatical import Analizador
from analizador.analizador import Analizador as Arbol
from verificador.verificador import Verificador
from utils import archivos as utils
from explorador.Explorador import Explorador
from generador.generador import Generador
import argparse

# - - - [ Parser ] - - -
from utils.archivos import readText

parser = argparse.ArgumentParser(description= 'Compilador Marciano')

parser.add_argument('-explorar', dest='explorador', action='store_true', help='ejecuta el explorador y retorna los componentes lexicos')

parser.add_argument('-analizar', dest='analizador', action='store_true', help='ejecuta el analizador y retorna los componentes lexicos')

parser.add_argument('-analizar-arbol', dest='arbol', action='store_true', help='ejecuta el analizador y retorna el arbol de sintaxis abstracta')

parser.add_argument('-verificar', dest='verificador', action='store_true', help='ejecuta el verificador y retorna el asa decorado')

parser.add_argument('-generar', dest='generador', action='store_true', help='ejecuta el generador y retorna el código en marciano')

parser.add_argument('archivo', help='Archivo con el codigo fuente')


# - - - [ Funcionalidad ] - - -
def marciano():
	args = parser.parse_args()

	if args.explorador:
		archivo = open(args.archivo,"r")
		exp = Explorador(archivo)
		exp.explorar()
		if exp.tiene_errores():
			exp.imprimir_errores()
		else:
			exp.imprimir_componentes()

	elif args.analizador:
		archivo = open(args.archivo,"r")
		exp = Explorador(archivo)
		exp.explorar()
		if exp.tiene_errores():
			exp.imprimir_errores()
		else:
			analizador = Analizador(exp.get_componentes())
			analizador.analizar()
		
	elif args.arbol:
		archivo = open(args.archivo,"r")
		exp = Explorador(archivo)
		exp.explorar()
		if exp.tiene_errores():
			exp.imprimir_errores()
		else:
			analizador = Arbol(exp.componentes)
			analizador.analizar()
			analizador.imprimir_asa()

	elif args.verificador:
		archivo = open(args.archivo,"r")
		exp = Explorador(archivo)
		exp.explorar()
		if exp.tiene_errores():
			exp.imprimir_errores()
		else:
			analizador = Arbol(exp.componentes)
			analizador.analizar()

			verificador = Verificador(analizador.asa)
			verificador.verificar()
			verificador.imprimir_asa()
			print("\n\n")
			verificador.imprimirTabla()

	elif args.generador:
		archivo = open(args.archivo,"r")
		exp = Explorador(archivo)
		exp.explorar()
		if exp.tiene_errores():
			exp.imprimir_errores()
		else:
			analizador = Arbol(exp.componentes)
			analizador.analizar()

			verificador = Verificador(analizador.asa)
			verificador.verificar()
			#verificador.imprimir_asa()
			# print("\n\n")
			# verificador.imprimirTabla()
			generador = Generador(verificador.asa, verificador.tabla_simbolos)
			generador.generar()
			# generador.imprimir_arbol()

# Llamada al programa
if __name__ == '__main__':
	marciano()