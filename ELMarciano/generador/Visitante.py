from utils.arbol import TipoNodo

class VisitantePython:

    tabuladores = 0

    def visitar(self, nodo :TipoNodo):
        
        resultado = ''

        if nodo.tipo is TipoNodo.PROGRAMA:
            resultado = self.__visitar_programa(nodo)

        elif nodo.tipo is TipoNodo.ASIGNACION:
            resultado = self.__visitar_asignacion(nodo)

        elif nodo.tipo is TipoNodo.EXPRESION_MATEMATICA:
            resultado = self.__visitar_expresion_matematica(nodo)

        elif nodo.tipo is TipoNodo.EXPRESION:
            resultado = self.__visitar_expresion(nodo)

        elif nodo.tipo is TipoNodo.FUNCION:
            resultado = self.__visitar_funcion(nodo)

        elif nodo.tipo is TipoNodo.INVOCACION:
            resultado = self.__visitar_invocacion(nodo)

        elif nodo.tipo is TipoNodo.PARAMETROS_INVOCACION:
            resultado = self.__visitar_parametros_invocacion(nodo)

        elif nodo.tipo is TipoNodo.PARAMETROS_FUNCION:
            resultado = self.__visitar_parametros_funcion(nodo)

        elif nodo.tipo is TipoNodo.INSTRUCCION:
            resultado = self.__visitar_instruccion(nodo)

        elif nodo.tipo is TipoNodo.REPETICION:
            resultado = self.__visitar_repeticion(nodo)

        elif nodo.tipo is TipoNodo.SI:
            resultado = self.__visitar_si(nodo)

        elif nodo.tipo is TipoNodo.TALVEZ:
            resultado = self.__visitar_talvez(nodo)

        elif nodo.tipo is TipoNodo.SINO:
            resultado = self.__visitar_sino(nodo)

        elif nodo.tipo is TipoNodo.OPERADOR_LOGICO:
            resultado = self.__visitar_operador_logico(nodo)

        elif nodo.tipo is TipoNodo.CONDICION:
            resultado = self.__visitar_condicion(nodo)

        elif nodo.tipo is TipoNodo.COMPARACION:
            resultado = self.__visitar_comparacion(nodo)

        elif nodo.tipo is TipoNodo.RETORNO:
            resultado = self.__visitar_retorno(nodo)

        elif nodo.tipo is TipoNodo.BLOQUE_INSTRUCCIONES:
            resultado = self.__visitar_bloque_instrucciones(nodo)

        elif nodo.tipo is TipoNodo.OPERADOR:
            resultado = self.__visitar_operador(nodo)

        elif nodo.tipo is TipoNodo.VALOR_VERDAD:
            resultado = self.__visitar_veracidad(nodo)

        elif nodo.tipo is TipoNodo.COMPARADOR:
            resultado = self.__visitar_operador_logico(nodo)

        elif nodo.tipo is TipoNodo.TEXTO:
            resultado = self.__visitar_texto(nodo)

        elif nodo.tipo is TipoNodo.ENTERO:
            resultado = self.__visitar_entero(nodo)

        elif nodo.tipo is TipoNodo.FLOTANTE:
            resultado = self.__visitar_flotante(nodo)

        elif nodo.tipo is TipoNodo.IDENTIFICADOR:
            resultado = self.__visitar_identificador(nodo)

        else:
            raise Exception('Error no deberia llegar aqui')

        return resultado

    def __visitar_programa(self, nodo_actual):
        
        instrucciones = []
        # Se ignoran los comentarios

        for nodo in nodo_actual.nodos:
            instrucciones.append(nodo.visitar(self))

        return '\n'.join(instrucciones) 

    def __visitar_asignacion(self, nodo_actual):
        """
        Asignacion ::= > (Literal | ExpresionMate | Invocacion )
        """

        resultado = """{} = {}"""

        instrucciones = []

        for nodo in nodo_actual.nodos:
            instrucciones.append(nodo.visitar(self))

        return resultado.format(instrucciones[0],instrucciones[1])

    def __visitar_expresion_matematica(self, nodo_actual):
        """
        ExpresionMate ::= ((Comparacion | Literal | Identificador))
        """

        instrucciones = []

        for nodo in nodo_actual.nodos:
            instrucciones += [nodo.visitar(self)]

        return ' '.join(instrucciones) 

    def __visitar_expresion(self, nodo_actual):
        """
        Expresion ::= ExpresionMate Operador ExpresionMate
        """

        instrucciones = []

        for nodo in nodo_actual.nodos:
            instrucciones += [nodo.visitar(self)]

        return ' '.join(instrucciones) 


    def __visitar_funcion(self, nodo_actual):
        """
        Funcion ::= -> (Parametros) { Instruccin + }
        """

        resultado = """\ndef {}({}):\n{}"""

        instrucciones = []

        for nodo in nodo_actual.nodos:
            instrucciones += [nodo.visitar(self)]

        return resultado.format(instrucciones[0],instrucciones[1], '\n'.join(instrucciones[2]))

    def __visitar_invocacion(self, nodo_actual):
        """
        Invocacion ::= .Identificador(Parametros)
        """

        resultado = """{}({})"""

        instrucciones = []

        for nodo in nodo_actual.nodos:
            instrucciones += [nodo.visitar(self)]

        return resultado.format(instrucciones[0], instrucciones[1])

    def __visitar_parametros_invocacion(self, nodo_actual):
        """
        Parametros ::= Valor (,Valor)+
        """
        parametros = []

        for nodo in nodo_actual.nodos:
            parametros.append(nodo.visitar(self))

        if len(parametros) > 0:
            return ','.join(parametros)

        else:
            return ''


    def __visitar_parametros_funcion(self, nodo_actual):
        """
        Parametros ::= Valor (,Valor)+
        """

        parametros = []

        for nodo in nodo_actual.nodos:
            parametros.append(nodo.visitar(self))

        if len(parametros) > 0:
            return ','.join(parametros)

        else:
            return ''



    def __visitar_instruccion(self, nodo_actual):
        """
        Instruccion ::= (Repeticion | Bifurcacion | Retorno | Error)
        """

        valor = ""

        for nodo in nodo_actual.nodos:
            valor = nodo.visitar(self)

        return valor


    def __visitar_repeticion(self, nodo_actual):
        """
        Repeticion ::= ^(Condicion) {Instrucciones}
        """

        resultado = """while {}:\n{}"""

        instrucciones = []

        # Visita la condicion
        for nodo in nodo_actual.nodos:
            instrucciones.append(nodo.visitar(self))

        return resultado.format(instrucciones[0],'\n'.join(instrucciones[1]))

    def __visitar_si(self, nodo_actual):
        """
        Bifurcacion ::= ?(condicion) {Instruccion+}
        """

        resultado = """if {}:\n{}"""

        instrucciones = []

        # Visita los dos nodos en el siguiente nivel si los hay
        for nodo in nodo_actual.nodos:
            # print(nodo.visitar(self))
            instrucciones.append(nodo.visitar(self))

        return resultado.format(instrucciones[0],'\n'.join(instrucciones[1]))

    def __visitar_talvez(self, nodo_actual):
        """
        DiaySi ::= ?:( Condicion ) {instruccion+}
        """

        resultado = """elif {}:\n{}"""

        instrucciones = []

        for nodo in nodo_actual.nodos:
            instrucciones.append(nodo.visitar(self))

        return resultado.format(instrucciones[0],'\n'.join(instrucciones[1]))

    def __visitar_sino(self, nodo_actual):
        """
        Sino ::= :(comparacion){instruccion+}
        """

        resultado = """else:\n  {}"""

        instrucciones = []

        for nodo in nodo_actual.nodos:
            instrucciones += [nodo.visitar(self)]

        return resultado.format('\n'.join(instrucciones[0]))


    def __visitar_condicion(self, nodo_actual):
        """
        Condicion ::= Comparacion (|Comparacion)+
        """

        resultado = """{} {} {}"""

        instrucciones = []

        for nodo in nodo_actual.nodos:
            instrucciones += [nodo.visitar(self)]

        if len(instrucciones) == 1:
            return resultado.format(instrucciones[0],'', '')
        else:
            return resultado.format(instrucciones[0],instrucciones[1],instrucciones[2])




    def __visitar_comparacion(self, nodo_actual):
        """
        Comparacion ::= Numero Comparador Numero | ValorVerdad Comparador ValorVerdad
        """

        resultado = '{} {} {}'

        elementos = []

        # Si los 'Valor' son identificadores se asegura que existan (IDENTIFICACIoN)
        for nodo in nodo_actual.nodos:
            elementos.append(nodo.visitar(self))

        return resultado.format(elementos[0], elementos[1], elementos[2])


    def __visitar_valor(self, nodo_actual):
        """
        Valor ::= (Identificador | Literal)
        """
        # En realidad núnca se va a visitar por que lo saqué del arbol
        # durante la etapa de analisiss

    def __visitar_retorno(self, nodo_actual):
        """
        Retorno ::= >> Valor+
        """

        resultado = 'return {}'
        valor = ''

        for nodo in nodo_actual.nodos:
            valor = nodo.visitar(self)

        return resultado.format(valor)
       
    def __visitar_error(self, nodo_actual):
        """
        Error ::= ~ Valor
        """
        resultado = 'print("\033[91m", {}, "\033[0m", file=sys.stderr)'
        valor = ''

        # Verifico si 'Valor' es un identificador que exista (IDENTIFICACIoN)
        for nodo in nodo_actual.nodos:
            valor = nodo.visitar(self)

        return resultado.format(valor)

    def __visitar_bloque_instrucciones(self, nodo_actual):
        """
        BloqueInstrucciones ::= Instruccion+
        """
        self.tabuladores += 2

        instrucciones = []

        # Visita todas las instrucciones que contiene
        for nodo in nodo_actual.nodos:
            instrucciones += [nodo.visitar(self)]

        instrucciones_tabuladas = []

        for instruccion in instrucciones:
            instrucciones_tabuladas += [self.__retornar_tabuladores() + instruccion]
            

        self.tabuladores -= 2

        return instrucciones_tabuladas

    def __visitar_operador(self, nodo_actual):
        """
        OperadorMatematico ::= (+ | - | * | /)
        """
        if nodo_actual.contenido == '+':
            return '+'

        elif nodo_actual.contenido == '-':
            return '-'

        elif nodo_actual.contenido == '*':
            return '*'

        elif nodo_actual.contenido == '/':
            return '/'

        else:
            return 'ERROR'


    def __visitar_valor_verdad(self, nodo_actual):
        """
        ValorVerdad ::= (T | F)
        """
        return nodo_actual.contenido
        

    def __visitar_operador_logico(self, nodo_actual):
        """
        Comparador ::= (> | < | = | != | <= | >= | && | || )
        """
        if nodo_actual.contenido == '>':
            return '>'

        elif nodo_actual.contenido == '<':
            return '<'

        elif nodo_actual.contenido == '=':
            return '=='

        elif nodo_actual.contenido == '!=':
            return '!='

        elif nodo_actual.contenido == '<=':
            return '<='

        elif nodo_actual.contenido == '>=':
            return '>='

        elif nodo_actual.contenido == '&&':
            return 'and'

        elif nodo_actual.contenido == '||':
            return 'otra'

        else:
            # No deberia de llegar aqui, pero
            # Para evitar que se caiga retorna error xd
            return 'ERROR OPERADOR LOGICO'


    def __visitar_texto(self, nodo_actual):
        """
        Texto ::= ~/\w(\s\w)*)?~
        """
        return nodo_actual.contenido.replace('~', '"')

    def __visitar_entero(self, nodo_actual):
        """
        Entero ::= (-)?\d+
        """
        return nodo_actual.contenido

    def __visitar_flotante(self, nodo_actual):
        """
        Flotante ::= (-)?\d+.(-)?\d+
        """
        return nodo_actual.contenido
        

    def __visitar_identificador(self, nodo_actual):
        """
        Identificador ::= [a-z][a-zA-Z0-9]+
        """
        return nodo_actual.contenido

    def __retornar_tabuladores(self):
        return " " * self.tabuladores
