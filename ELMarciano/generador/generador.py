from utils.arbol import ArbolSintaxisAbstracta
from generador.Visitante import VisitantePython
from verificador.TablaSimbolos import TablaSimbolos

class Generador:

    arbol_ASA: ArbolSintaxisAbstracta
    visitante: VisitantePython

    ambiente_std = """# - - - [ Ambiente Estandar ] - - -

def conecte(texto1, texto2):
    return texto1 + texto2

def sector(algo, indice):
    return algo[indice]

def metrica(algo):
    return len(algo)

def _(texto):
    print(texto)

def aborda():
    return input()

# - - - [ FIN Ambiente Estandar ] - - -
"""

    def __init__(self, nuevo_arbol: ArbolSintaxisAbstracta, nueva_tabla: TablaSimbolos):
        self.arbol_ASA = nuevo_arbol
        self.visitante = VisitantePython()

    def imprimir_arbol(self):
        # Imprime el Árbol ASA

        if self.arbol_ASA.raiz is None:
            print([])
        else:
            self.arbol_ASA.imprimir_preorden()

    def generar(self):
        resultado = self.visitante.visitar(self.arbol_ASA.raiz)
        resultado += """

if __name__ == '__main__':
    main()
        """
        print(self.ambiente_std)
        print(resultado)